export function calculateMove(
  current: number,
  state: number[],
  totalPage: number
): number[] | null {
  let mid = Math.floor(state.length / 2);
  let arr;
  let minus = current - state[mid];
  if (minus === 0) {
    arr = null;
  } else if (minus > 0) {
    let tmp = state[state.length - 1];
    if (tmp + minus < totalPage) {
      arr = state.map((v) => v + minus);
    } else {
      if (tmp === totalPage) {
        arr = null;
      } else {
        arr = state.map((v) => v + totalPage - tmp);
      }
    }
  } else {
    //负数
    if (state[0] + minus > 1) {
      arr = state.map((v) => v + minus);
    } else {
      //边缘，看最大能减几
      if (state[0] === 1) {
        arr = null;
      } else {
        arr = state.map((v) => v - state[0] + 1);
      }
    }
  }
  return arr;
}
