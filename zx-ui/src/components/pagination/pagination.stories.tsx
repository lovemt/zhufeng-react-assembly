import React, { useState } from "react";

import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";

import { Pagination } from "./index";
export default {
  title: "Pagination",
  component: Pagination,
  decorators: [withKnobs],
};

function callback() {}

export const knobsPagination = () => (
  <Pagination
    defaultCurrent={number("defualtCurrent", 1)}
    total={number("total", 100)}
    barMaxSize={number("barMaxSize", 5)}
    pageSize={number("pageSize", 5)}
    callback={action("callback")}
  ></Pagination>
);
