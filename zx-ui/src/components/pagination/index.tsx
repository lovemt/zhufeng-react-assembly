import React, { useState, useMemo, useEffect, CSSProperties } from "react";

import { PageUl } from "./index.styled";

import Button from "../button";
import { Icon } from "../icon";
import { calculateMove } from "./utils";
import { color } from "../shared/styles";

export type PaginationProps = {
  /** 每页显示多少条*/
  pageSize?: number;
  /** 默认显示第几页 */
  defaultCurrent?: number;
  /** 总共条数*/
  total: number;
  /** 分页条目最大显示长度 */
  barMaxSize?: number;
  /** 回调页数 */
  callback?: (v: number) => void;
  /** 外层style*/
  style?: CSSProperties;
  /**外层类名 */
  classnames?: string;
};

export function Pagination({ callback }: PaginationProps) {
  const pageSize = 10;
  const defaultCurrent = 1;
  const barMaxSize = 5;
  const total = 1000;
  const [current, setCurrent] = useState(defaultCurrent);
  const [state, setState] = useState<Array<number>>([]);
  const totalPage = useMemo(() => {
    let number = Math.ceil(total / pageSize);
    if (number > barMaxSize) {
      let statetmp = new Array(barMaxSize).fill(1).map((_x, y) => y + 1);
      setState(statetmp);
      let arr = calculateMove(defaultCurrent, statetmp, number);
      if (arr) {
        setState(arr);
      }
    } else {
      let statetmp = new Array(number).fill(1).map((_x, y) => y + 1);
      setState(statetmp);
      let arr = calculateMove(defaultCurrent, statetmp, number);
      if (arr) {
        setState(arr);
      }
    }
    return number;
  }, [pageSize, total]);

  useEffect(() => {
    if (callback) callback(current);
  }, [callback, current]);

  console.log(totalPage);
  return (
    <PageUl style={{ display: "flex" }}>
      <li>
        <Button
          appearance="primaryOutline"
          disabled={current === 1 ? true : false}
          onClick={() => {
            if (state.length > 0) {
              if (state[0] > 1) {
                let statetmp = state.map((x) => x - 1);
                setState(statetmp);
                setCurrent(current - 1);
                let arr = calculateMove(current - 1, statetmp, totalPage);
                if (arr) {
                  setState(arr);
                }
              } else if (current !== state[0]) {
                setCurrent(current - 1);
                let arr = calculateMove(current - 1, state, totalPage);
                if (arr) {
                  setState(arr);
                }
              }
            }
          }}
        ></Button>
      </li>
      {state.map((x, i) => {
        return (
          <li key={i}>
            <Button
              appearance={current === x ? "primary" : "primaryOutline"}
              onClick={() => {
                setCurrent(x);
                let arr = calculateMove(x, state, totalPage);
                if (arr) {
                  setState(arr);
                }
              }}
            >
              {x}
            </Button>
          </li>
        );
      })}
      <li>
        <Button
          appearance="primaryOutline"
          disabled={current === totalPage ? true : false}
          onClick={() => {
            if (state.length > 0) {
              if (state[barMaxSize! - 1] < totalPage) {
                let statetmp = state.map((x) => x + 1);
                setState(statetmp);
                setCurrent(current + 1);
                let arr = calculateMove(current + 1, statetmp, totalPage);
                if (arr) {
                  setState(arr);
                }
              } else {
                if (current !== totalPage) {
                  setCurrent(current + 1);
                  let arr = calculateMove(current + 1, state, totalPage);
                  if (arr) {
                    setState(arr);
                  }
                }
              }
            }
          }}
        />
      </li>
    </PageUl>
  );
}

Pagination.defaultProps = {
  pageSize: 10,
  defaultCurrent: 11,
  barMaxSize: 5,
  total: 1000,
};
