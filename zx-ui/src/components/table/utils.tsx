import React from "react";
import { SourceDataType, ColumnType } from "./index";

export const MapData = (data: SourceDataType[], columnData: ColumnType[]) => {
  return data.map((v) => {
    return (
      <tr key={v.key}>
        {columnData.map((value, index) => {
          return (
            <td key={index}>
              <span>
                {value.render
                  ? value.render(v[value.dataIndex], v, value)
                  : v[value.dataIndex]}
              </span>
            </td>
          );
        })}
      </tr>
    );
  });
};
