## Bonus彩蛋

本次训练营最终章，给大家带来一个彩蛋组件— keep-alive。

这个组件的原理很特别，因为并不是一种常规思路，所以我觉得拿来做训练营的彩蛋真的再适合不过了。

为了照顾不懂啥是keep-alive的同学，先举个简单例子：

```tsx
import React, { useState } from "react";
import "./App.css";

function Counter() {
	const [state, setState] = useState(0);
	return (
		<div>
			{state}
			<button
				onClick={() => {
					setState((v) => v + 1);
				}}
			>
				++++
			</button>
		</div>
	);
}

function App() {
	const [show, setShow] = useState(true);
	return (
		<div className="App">
			<button onClick={() => setShow(!show)}>切换</button>
			<div>
				无keepalive
				{show && <Counter></Counter>}
			</div>
			<div>
				无keepalive
				{show && <Counter></Counter>}
			</div>
		</div>
	);
}

export default App;
```

很显然，目前啥都没有，如果点击counter，它自己的状态变了，再点击app的show切换，一切又变成0。

这里就需要解决show切换时，有keepAlive的组件它不是0。

### 代码实现

```jsx
import React, { useState } from "react";
import { render } from "react-dom";
import KeepAlive, { AliveScope } from "./keep";

function Counter() {
	const [count, setCount] = useState(0);
	return (
		<div>
			count: {count}
			<button onClick={() => setCount((count) => count + 1)}>add</button>
		</div>
	);
}

function App() {
	const [show, setShow] = useState(true);
	return (
		<AliveScope>
			<div>
				<button onClick={() => setShow((show) => !show)}>Toggle</button>
				<p>无 KeepAlive</p>
				{show && <Counter />}
				<p>有 KeepAlive</p>
				{show && (
					<KeepAlive id="Test">
						<Counter />
					</KeepAlive>
				)}
			</div>
		</AliveScope>
	);
}

render(<App />, document.getElementById("root"));
```

keep.js

```jsx
import React, {
	createContext,
	useState,
	useEffect,
	useRef,
	useContext,
	useMemo,
} from "react";

const Context = createContext();

export function AliveScope(props) {
	const [state, setState] = useState({});
	const ref = useMemo(() => {
		return {};
	}, []);
	const keep = useMemo(() => {
		return (id, children) =>
			new Promise((resolve) => {
				setState({
					[id]: { id, children },
				});
				setTimeout(() => {
					//需要等待setState渲染完拿到实例返回给子组件。
					resolve(ref[id]);
				});
			});
	}, [ref]);
	return (
		<Context.Provider value={keep}>
			{props.children}
			{Object.values(state).map(({ id, children }) => (
				<div
					key={id}
					ref={(node) => {
						ref[id] = node;
					}}
				>
					{children}
				</div>
			))}
		</Context.Provider>
	);
}

function KeepAlive(props) {
	const keep = useContext(Context);
	useEffect(() => {
		const init = async ({ id, children }) => {
			const realContent = await keep(id, children);
			if (ref.current) {
				ref.current.appendChild(realContent);
			}
		};
		init(props);
	}, [props, keep]);
	const ref = useRef(null);
	return <div ref={ref} />;
}

export default KeepAlive;
```

### 原理

先要分清2层概念，一个是fiber树上的显示，一个是真实dom的展示。

由于fiber树不强检测是否跟真实dom匹配，利用这个特性，可以控制组件的渲染状态。

keepalive组件可以拿到要渲染的虚拟dom，将其转移至alivescope组件，alivescope就是前面说的父组件，keepalive就是前面说的子组件。

父组件拿到子组件传来的虚拟dom进行渲染，这样，在其fiber树上就会表示这个组件已经被其渲染了。

关键点来了，渲染了后，父组件就可以获取这个组件dom，再把dom传给子组件，子组件获取dom后，插入自己已渲染的dom里。

这样就会导致，父组件渲染了dom，并且有这个dom的fiber，享受父组件的生命周期，但是渲染却是按照子组件渲染的逻辑走（就跟key错误误删一个道理），当子组件卸载时，fiber会commit掉子组件的dom，当然子组件fiber没有记录父组件有个dom跑子组件下了，结果这个dom就一起跟着被干掉了。

实际fiber树上仍然有这个dom，因为这个dom在父组件。

当子组件重新显示时，子组件dom加载完毕后，会调用父组件方法，重新获取父组件目前已经渲染的dom，有人可能奇怪，这个dom不是已经被干掉了？实际dom会缓存在fiber里，前面删掉的那个只是其中一个引用。

这样子组件又把父组件传来的dom给渲染出来了，而且是未卸载的状态。



## 参考答案

此训练营参考答案仓库：https://github.com/yehuozhili/atom-design-explorer

我的微信号是h1637830449，欢迎一起讨论，共同进步。

